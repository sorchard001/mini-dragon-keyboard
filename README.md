## Mini Keyboard for the Dragon 32 and 64

Mini Dragon 32/64 compatible keyboard based on 6x6x7mm through-hole mount tactile switches.

Adds four new keys F1-F4 to the otherwise unused positions in the keyboard matrix.

Two KiCad 7 designs are included: The first is the main keyboard while the second is a top cover with legend. The two boards should be spaced 4 to 5mm apart. A low profile right-angle pin header should be selected to fit in the gap between the boards. Alternatively solder wires directly to the header pads. Both boards are pretty small at 120 x 60mm, which makes for low shipping costs from the pcb fab.

The manufacturing files can be found in the plots folder.

To be more practical this could use some sort of bottom cover over the soldered joints, but otherwise works pretty well.

<img src="images/1.jpg" width="500">

<img src="images/2.jpg" width="500">


